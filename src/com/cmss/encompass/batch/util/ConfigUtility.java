/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.encompass.batch.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Godwin
 */
public class ConfigUtility {

    public static Boolean isClearData = false;
    public static final String REPORT_DIR = "E:\\Dakshata\\";
//    public static final String REPORT_DIR = "C:/Users/admin/Desktop/L&TReport/";
    public static final String REPORT_VIEW_TEMPLATE_DIR = "ppt/";
//    public static final String REPORT_VIEW_TEMPLATE_NAME = "Perception_EL2_V1";
    public static final String REPORT_VIEW_TEMPLATE_NAME = "templateidpl";
//    public static final String REPORT_VIEW_TEMPLATE_NAME = "L&TReport";
//    public static final String REPORT_VIEW_TEMPLATE_NAME = "LLTT";
    public static final String REPORT_VIEW_TEMPLATE_EXT = ".pptx";
    public static final String REPORT_DATA_TEMPLATE_DIR = "excel/";
    public static final String REPORT_DATA_TEMPLATE_NAME = "template";
    public static final String REPORT_DATA_TEMPLATE_EXT = ".xlsx";

    public static void unZipIt(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];

        try {

            //create output directory is not exists
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            //get the zip file content
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(zipFile));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                //System.out.println("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            //System.out.println("Done");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static boolean replaceTextInFiles(String strFolderPath, String strFindText, String strReplacementText) throws FileNotFoundException, IOException {

        //System.out.println("Enter the location of folder:");


        File file = new File(strFolderPath);
        
        File[] filenames = file.listFiles();
        StringBuilder sbword = new StringBuilder();

        String line = null;
        if(filenames!=null)
        for (File file1 : filenames) {
            //System.out.println("File name" + file1.toString());
            if(file1.isDirectory()){
                continue;
            }
            sbword.setLength(0);
            BufferedReader br = new BufferedReader(new FileReader(file1));

            line = br.readLine();

            while (line != null) {
                //System.out.println(line);
                sbword.append(line).append("\r\n");
                line = br.readLine();
            }
            br.close();

            replaceLines(sbword, strFindText, strReplacementText);
            writeToFile(file1.toString(), sbword);

        }

        return true;
    }

    private static void replaceLines(StringBuilder sbword, String strFindText, String strReplacementText) {
        replaceAll(sbword, strFindText, strReplacementText);
    }

    private static void replaceAll(StringBuilder builder, String from, String to) {
        int index = builder.indexOf(from);
        while (index != -1) {
            builder.replace(index, index + from.length(), to);
            index += to.length();
            index = builder.indexOf(from, index);
        }
    }

    private static void writeToFile(String filename, StringBuilder sbword) throws IOException {
        try {
            File file1 = new File(filename);
            BufferedWriter bufwriter = new BufferedWriter(new FileWriter(file1));
            bufwriter.write(sbword.toString());
            bufwriter.close();

        } catch (Exception e) {
            CustomLogger.error(e);
            //System.out.println("Error occured while attempting to write to file: " + e.getMessage());
        }

    }

    public static void createZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

        //System.out.println("Writing '" + fileName + "' to zip file");

        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

    public static void listFilesForFolder(final File folder,List<String> lstFiles) {
        
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry,lstFiles);
            } else {
                lstFiles.add(fileEntry.getAbsoluteFile().getAbsolutePath());
                    //System.out.println(fileEntry.getAbsoluteFile().getAbsolutePath());
            }
        }
    }

    public static void addToZipFile(String folderName,String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

        //System.out.println("Writing '" + folderName+fileName + "' to zip file");

        File file = new File(folderName+fileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }
    
    
    public static boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
          //System.out.println("files"+files[i].getAbsolutePath());
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
             //System.out.println(""+files[i].getAbsolutePath()+":"+files[i].delete());
             files[i].delete();
            
         }
      }
    }
    return( path.delete() );
  }
    
    
    public static Double computePercentile(Double rateeScore, Double[] drr) {

//        if (drr.length < 2) {
//            return Double.NaN;
//        }
//        Double score = drr[0];
        Double counter = 0D;
        Arrays.sort(drr);
        for (int i = 1; i < drr.length; ++i) {
            if (drr[i] <= rateeScore) {
                counter++;
            }
        }

        return 100.0 * counter / (drr.length - 1);

    }
    
    public static Double getPercentile(List<Double> values, double lowerPercent) {
//        if (values == null || values.isEmpty()) {
//            throw new IllegalArgumentException("The data List either is null or does not contain any data.");
//        }
//
//        // Rank order the values
//        List<Double> v = values;
//        Collections.sort(v);
//        int n = (int) Math.ceil(v.size() * lowerPercent / 100);
//        return v.get(n-1);
//    }

 if (values == null || values.isEmpty()) {
            throw new IllegalArgumentException("The data List either is null or does not contain any data.");
        }

        // Rank order the values
        List<Double> v = values;
        Collections.sort(v);

        // R = P/100 x (N + 1)
        double lowerPercentby100 = (double) lowerPercent / 100;

        double R = (lowerPercentby100 * (v.size() + 1));
        //System.out.println("R= " + R);

        if (R == Math.round(R)) {
            Double result = v.get((int) (R - 1));
            //System.out.println("percentile is: " + v.get((int) (R - 1)));
            return result;
        } else {

            int IR = (int) R;
        //    System.out.println("IR= " + IR);
            float FR = (float) (R - IR);
        //    System.out.println("FR= " + FR);
            
            Double result = (((FR) * (v.get(IR) - v.get((IR - 1)))) + v.get((IR - 1)));
        //    System.out.println("percentile is: " + result);
            return result;

        }
    }

}
