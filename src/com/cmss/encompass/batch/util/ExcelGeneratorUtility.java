/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.encompass.batch.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Godwin
 */
public class ExcelGeneratorUtility {

    XSSFWorkbook workbook = null;
    String strReportFilePath = null;

    public ExcelGeneratorUtility(String strReportFilePath) {
        this.strReportFilePath = strReportFilePath;
        try {
            System.out.println("strReportFilePath try:" + strReportFilePath);
            FileInputStream fileInputStream = new FileInputStream(new File(strReportFilePath));
            workbook = new XSSFWorkbook(fileInputStream);
        } catch (Exception ex) {
            System.out.println("strReportFilePath error:" + strReportFilePath);
            CustomLogger.error(ex);
        }
    }

    public void closeExcel() {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(strReportFilePath));
            workbook.write(fos);
        } catch (Exception ex) {
            CustomLogger.error(ex);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (Exception e) {
                CustomLogger.error(e);
            }
        }
    }

    public void printToSheet(int intSheetNo, List<Object[]> lstOfRows) {
        printToSheet(intSheetNo, lstOfRows, 1, 0);
    }

    public void printToSheet(int intSheetNo, List<Object[]> lstOfRows, int intRowStart, int intColStart) {
        printToSheet(intSheetNo, lstOfRows, intRowStart, intColStart, false);
    }

    public void printToSheet(int intSheetNo, List<Object[]> lstOfRows, int intRowStart, int intColStart, Boolean colorCell) {

        XSSFSheet sheet = workbook.getSheetAt(intSheetNo);
        System.out.println("lstOfRows"+lstOfRows.size());
        
       
        for (int intI = 0; intI < lstOfRows.size(); intI++) {
            Object[] objRow = lstOfRows.get(intI);

            System.out.println("in print sheet :"+objRow[0]);


            System.out.println("row: "+objRow.length);

            XSSFRow row = sheet.getRow((intI + intRowStart));

//            System.out.println("3: "+row.getCell(3));
    //        System.out.println("2:"+row.getCell(2));

            System.out.println("3: "+row.getCell(3));
            System.out.println("2:"+row.getCell(2));

          

            for (int intJ = 0; intJ < objRow.length; intJ++) {
                System.out.println(intJ+intColStart);
                XSSFCell cell = row.getCell((intJ + intColStart));
                            
                if (colorCell && intJ == 0) {
                    List<Float> lstElements = new ArrayList<Float>();
                    for (int intK = 0; intK < lstOfRows.size(); intK++) {
                        lstElements.add(Float.parseFloat(((Object[]) lstOfRows.get(intK))[4].toString()));
                    }
                    Collections.sort(lstElements);
                    if (Float.parseFloat(objRow[4].toString()) == lstElements.get(0)) {
                        XSSFCellStyle style1 = (XSSFCellStyle)cell.getCellStyle().clone();
                        style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(200, 80, 79)));
                        style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
//                        style1.setBorderLeft(Short.parseShort("1"));
//                        style1.setBorderBottom(Short.parseShort("1"));
                        cell.setCellStyle(style1);
                    }
                    if (Float.parseFloat(objRow[4].toString()) == lstElements.get(lstElements.size() - 1)) {
                        XSSFCellStyle style1 = (XSSFCellStyle)cell.getCellStyle().clone();
                        style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(155, 179, 166)));
//                        style1.setBorderLeft(Short.parseShort("1"));
//                        style1.setBorderBottom(Short.parseShort("1"));
                        style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
                        cell.setCellStyle(style1);
                    }


                }
                boolean blnNumber=false;
                try{
                    blnNumber=true;
                    Double.parseDouble(objRow[intJ].toString());
                }catch(Exception e){
                    blnNumber=false;
                }
               
                if (objRow[intJ]==null){
                    cell.setCellValue("-");
                }
                else if(blnNumber){
                    cell.setCellValue(Double.parseDouble(objRow[intJ].toString()));
                }
                else if(objRow[intJ].getClass().getSimpleName().equals("String")) {
                    cell.setCellValue(objRow[intJ].toString());
                } else if (objRow[intJ].getClass().getSimpleName().equals("Float")) {
                    cell.setCellValue(Double.parseDouble(objRow[intJ].toString()));
                } else if (objRow[intJ].getClass().getSimpleName().equals("Double")) {
                    cell.setCellValue(Double.parseDouble(objRow[intJ].toString()));
                }
            }
        }

        //System.out.println("intSheetNo"+intSheetNo);
//        for (Object[] result : lstOfRows) {
//            for(Object rs:result){
//                System.out.println("Class Type"+rs.getClass().getSimpleName());
//                System.out.println("result"+rs.toString());
//            }
//        }
        //System.out.println("intSheetNo" + intSheetNo + ":" + lstOfRows.size());
    }
}
