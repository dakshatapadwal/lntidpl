/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.encompass.batch.task;

import com.aspose.slides.AsposeLicenseException;
import com.aspose.slides.IParagraph;
import com.aspose.slides.IPortion;
import com.aspose.slides.ITextFrame;
import com.aspose.slides.Presentation;
import com.aspose.slides.SaveFormat;
import com.aspose.slides.SlideUtil;
import com.cmss.encompass.batch.dao.PrintReportDAO;
import com.cmss.encompass.batch.util.ConfigUtility;
import com.cmss.encompass.batch.util.CustomLogger;
import com.cmss.encompass.batch.util.ExcelGeneratorUtility;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Godwin
 */
@Component
@Scope("prototype")
public class PrintReportTask implements Runnable {

    Long lngUserId=2421L;
    Long lngCycleSequenceId=53L;
    PrintReportDAO printReportDAO;

    public void setConfig(Long lngUserId, Long lngCycleSequenceId, PrintReportDAO printReportDAO) {
        this.lngUserId = lngUserId;
        this.lngCycleSequenceId = lngCycleSequenceId;
        this.printReportDAO = printReportDAO;
    }

    @Override
    public void run() {

        
         try {
            FileUtils.copyFile(
                    new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
                            + ConfigUtility.REPORT_VIEW_TEMPLATE_NAME + ConfigUtility.REPORT_VIEW_TEMPLATE_EXT),
                    new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
                            + lngUserId.toString() + ConfigUtility.REPORT_VIEW_TEMPLATE_EXT), true);

//                FileUtils.copyFile(
//                        new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
//                        + ConfigUtility.REPORT_DATA_TEMPLATE_NAME + ConfigUtility.REPORT_DATA_TEMPLATE_EXT),
//                        new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
//                        + lngUserId.toString() + ConfigUtility.REPORT_DATA_TEMPLATE_EXT), true);
        } catch (Exception e) {
            CustomLogger.error(e);
        }

        //}
        //List<Object[]> lst = null;
//        ExcelGeneratorUtility excelGeneratorUtility =
//                new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_DATA_TEMPLATE_DIR
//                + lngUserId.toString() + ConfigUtility.REPORT_DATA_TEMPLATE_EXT);

        /*make modifications to pptx*/
        try {
            FileUtils.copyFile(
                    new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
                            + lngUserId.toString() + ConfigUtility.REPORT_VIEW_TEMPLATE_EXT),
                    new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                            + lngUserId.toString() + ".zip"), true);

            ConfigUtility.unZipIt(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                    + lngUserId.toString() + ".zip",
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/");

            File file = new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR
                    + lngUserId.toString() + "/"
                    + lngUserId.toString() + ".zip");
            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }

            file = new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + ".pptx");
            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
       
        
        
        
        
        List<Object[]> lstData = null;
        ExcelGeneratorUtility excelGeneratorUtility = null;
 //       List<Object[]> listofobjectarray = new ArrayList();
//       List<Object> listofobjects = new ArrayList();
        
        System.out.println("in run method");
        lstData = printReportDAO.getStatuswiseResponseCount(lngUserId, lngCycleSequenceId);
        System.out.println(lstData.size() + "=lst data");
        int intRespondent = 0;
      //  Object objRow[]=lstData.toArray();

        //  for (int intI = 0; intI < lstData.size(); intI++) {
        Object[] objRow1 = lstData.get(0);
        String remaining = objRow1[0].toString();
        int remainingcount = Integer.parseInt(remaining);

        Object[] objRow2 = lstData.get(1);
        String completed = objRow2[0].toString();
        int completedcount = Integer.parseInt(completed);
        int totalrespondents = remainingcount + completedcount;
         float percentage=((float)completedcount / totalrespondents) * 100;
            System.out.println(percentage);
        try {
            ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "no_responses", completed);
            ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "no_respondents", String.valueOf(totalrespondents));
            ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "true_percentage", String.valueOf(Double.parseDouble(String.valueOf(percentage))));
             ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "impIndex_total_invitees", String.valueOf(completed));
              ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "impIndex_total_responses", String.valueOf(totalrespondents));
               ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "impIndex_true_size %", String.valueOf(Double.parseDouble(String.valueOf(percentage))));
                ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "satIndex_total_invitees", String.valueOf(completed));
                 ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "satIndex_total_responses", String.valueOf(totalrespondents));
                  ConfigUtility.replaceTextInFiles(
                    ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/" + "ppt/slides/",
                    "satIndex_true_size %",String.valueOf(Double.parseDouble(String.valueOf(percentage))));
                 
                  
             
             
        } catch (IOException e) {
            e.printStackTrace();
        }


//        ********************************************************************************************************
        /*Importance based Competencywise average Slide 4 bar Chart left side*/

      excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                + "ppt/embeddings/Microsoft_Excel_Worksheet3.xlsx");
        lstData = printReportDAO.getImptbasedCompetencywiseAvg(lngUserId, lngCycleSequenceId);
        List<Object[]> listofobjects = new ArrayList();
       //Object[] obj= new Object[1];
     //   obj[0]=(Object)"Overall average score";
      //  listofobjects.add(obj);
      
      for (Object[] o:lstData)
                System.out.println(o[0].toString());
        
        for (int intI = 0; intI < lstData.size(); intI++) {
            Object[] objRow = lstData.get(intI);
            
            Object[] temp=new Object[1];
            temp[0]=objRow[1];
         
            listofobjects.add(temp);
            temp=null;
        }
        
        for (Object[] a : listofobjects) {
            System.out.println(a[0].toString());
        }
        System.out.println("*****************************************");      
        excelGeneratorUtility.printToSheet(0, listofobjects,1,1);
                    excelGeneratorUtility.closeExcel(); 

      excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                + "ppt/embeddings/Microsoft_Excel_Worksheet3.xlsx");
        lstData = printReportDAO.getImptbasedCompetencywiseAvg(lngUserId, lngCycleSequenceId);
      //  List<Object[]> listofobjects = new ArrayList();
       //Object[] obj= new Object[1];
     //   obj[0]=(Object)"Overall average score";
      //  listofobjects.add(obj);
        for (int intI = 0; intI < lstData.size(); intI++) {
            Object[] objRow = lstData.get(intI);
            
            Object[] temp=new Object[1];
            temp[0]=objRow[1];
         
            listofobjects.add(temp);
            temp=null;
        }
        
        for (Object[] a : listofobjects) {
            System.out.println(a[0].toString());
        }
        System.out.println("*****************************************");      
        excelGeneratorUtility.printToSheet(0, listofobjects,2,2);
                    excelGeneratorUtility.closeExcel(); 

//     excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
//                + "ppt/embeddings/Microsoft_Excel_Worksheet3.xlsx");
//        lstData = printReportDAO.getImptbasedCompetencywiseAvg(lngUserId, lngCycleSequenceId);
//        List<Object[]> listofobjects = new ArrayList();
//      
//        Object[] ob=new Object[lstData.size()];
//        for (int intI = 0; intI < lstData.size(); intI++) {
//            Object[] objRow = lstData.get(intI);
//            
//            Object temp=new Object();
//            temp=objRow[1];
//            ob[intI]=temp;
//            
//            temp=null;
//        }
//        listofobjects.add(ob);
//        for (Object a : ob) {
//            System.out.println(a.toString());
//        }
//        
//        System.out.println("*****************************************");      
//       excelGeneratorUtility.printToSheet(0, listofobjects,1,1);
//                   excelGeneratorUtility.closeExcel(); 

//        ********************************************************************************************************
        /*  Importance based Questionwise average Slide 4 bar Chart right side*/
//          excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
//                   + "ppt/embeddings/Microsoft_Excel_Worksheet4.xlsx");
//      lstData = printReportDAO.getImptbasedQuestionwiseAvg(lngUserId, lngCycleSequenceId);
//          List<Object[]> imptobjectarray = new ArrayList();
//        //  Object[] questionavgarray=new Object[lstData.size()];
//        for (int intI = 0; intI < lstData.size(); intI++) {
//            Object[] objRow = lstData.get(intI);
//       
//           Object[] object = new Object[2];
//             object[0]=objRow[1];
//             object[1]=objRow[2];
//          // questionavgarray[intI]=object;
//              imptobjectarray.add(object);
//            object=null;
//        }
//        
//        System.out.println("importance based questionwise avg");
//       for (Object[] a : imptobjectarray) {
//           System.out.println(a[0].toString());
//                   System.out.println(a[1].toString());
//        }
//        System.out.println("*****************************************");
//        excelGeneratorUtility.printToSheet(0, imptobjectarray, 1, 0);
//             excelGeneratorUtility.closeExcel(); 

//        ********************************************************************************************************
        /*  Satisfaction based Questionwise average Statisfaction Scale   Slide 5      */
    /*          excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                  + "ppt/embeddings/Microsoft_Excel_Worksheet2.xlsx");
       lstData = printReportDAO.getSatisfactionbasedQuestionwiseAvg(lngUserId, lngCycleSequenceId);
        List<Object[]> satisfactionobjectarray = new ArrayList();
        for (int intI = 0; intI < lstData.size(); intI++) {
            Object[] objRow = lstData.get(intI);
       
           Object[] object = new Object[2];
             object[0]=objRow[1];
             object[1]=objRow[2];
            satisfactionobjectarray.add(object);
            object=null;
        }
        System.out.println("importance based questionwise avg");
       for (Object[] a : satisfactionobjectarray) {
           System.out.println(a[0].toString());
                   System.out.println(a[1].toString());
        }
        System.out.println("*****************************************");
          excelGeneratorUtility.printToSheet(0, lstData, 1, 0);
         excelGeneratorUtility.closeExcel();       */ 
//        ********************************************************************************************************
        /*  Satisfaction based Competency average Statisfaction Scale   Slide 5      */
        excelGeneratorUtility = new ExcelGeneratorUtility(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/"
                + "ppt/embeddings/Microsoft_Excel_Worksheet6.xlsx");
        lstData = printReportDAO.getSatisfactionbasedCompetencywiseAvg(lngUserId, lngCycleSequenceId);
        List<Object[]> listofobjects1 = new ArrayList();
       //Object[] obj= new Object[1];
     //   obj[0]=(Object)"Overall average score";
      //  listofobjects.add(obj);
        
             
//        Object[] ob1=new Object[lstData.size()];
//        for (int intI = 0; intI < lstData.size(); intI++) {
//            Object[] objRow = lstData.get(intI);
//            
//            Object temp=new Object();
//            temp=objRow[1];
//            ob1[intI]=temp;
//            
//            temp=null;
//        }
//        listofobjects1.add(ob1);
//        for (Object a : ob1) {
//            System.out.println(a.toString());
//        }
//        
//        System.out.println("*****************************************");      
//       excelGeneratorUtility.printToSheet(0, listofobjects1,1,1);
//                   excelGeneratorUtility.closeExcel();  
//        
        FileOutputStream fos = new FileOutputStream(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + ".pptx");
            ZipOutputStream zos = new ZipOutputStream(fos);

            List<String> lstFiles = new ArrayList<String>();
            ConfigUtility.listFilesForFolder(new File(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString()), lstFiles);
            System.out.println("lstFiles.size()" + lstFiles.size());
            for (int intI = 0; intI < lstFiles.size(); intI++) {
                String strFileName = lstFiles.get(intI);
                ConfigUtility.addToZipFile(ConfigUtility.REPORT_DIR + ConfigUtility.REPORT_VIEW_TEMPLATE_DIR + lngUserId.toString() + "/",
                        strFileName.substring(strFileName.indexOf(lngUserId.toString()) + lngUserId.toString().length() + 1), zos);
            }
            zos.close();
            fos.close();
        
        
        
    }
catch (Exception e) {
            CustomLogger.error(e);
        }
    }
    
    
}
