/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.encompass.batch.dao;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author cm-012
 */
public class PrintReportDAOImpl extends HibernateDaoSupport implements PrintReportDAO {

    @Override
    public List<Object[]> getStatuswiseResponseCount(Long lngUserId, Long lngCycleSequenceId) {
        System.out.println("in dao method");
        return (List<Object[]>) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                SQLQuery query = session.createSQLQuery("select count(feedback_status),feedback_status from lntidpl.encompass_mpng_cycle_sequence_ratee_rater"
                        + " where cycle_sequence_id =:cycle_sequence_id "
                        + " and rater_id in (select user_id from lntidpl.custom_users_master where manager_full_name_lower_case="
                        + " (select employee_full_name_lower_case from lntidpl.custom_users_master where user_id=:ratee_id))"
                        + " group by feedback_status");
                query.setParameter("cycle_sequence_id", lngCycleSequenceId);
                query.setParameter("ratee_id", lngUserId);

                List list = query.list();

//                for (int i = 0; i < list.size(); i++) {
//                    System.out.println(list.get(i));
//                }



                return list;
            }
        });

    }

    
    public List<Object[]> getImptbasedCompetencywiseAvg(Long lngUserId, Long lngCycleSequenceId) {

        return (List<Object[]>) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                SQLQuery query = session.createSQLQuery("select competency_name,round(avg(question_impt_rating),2)" + " from (" + "  select "
                        + " (select competency_name from lntidpl.encompass_mstr_competency c where competency_id=(select q.competency_id from lntidpl.encompass_mstr_question q where q.question_id=t.question_id)) competency_name,"
                        + " t.rater_id,question_impt_rating,question_freq_rating from lntidpl.encompass_trxn_feedback t"
                        + " where cycle_sequence_id =:cycle_sequence_id and rater_id in "
                        + "("
                        + " select rater_id from lntidpl.encompass_mpng_cycle_sequence_ratee_rater where rater_id in ("
                        + " select user_id from lntidpl.custom_users_master where manager_full_name_lower_case="
                        + " (select employee_full_name_lower_case from lntidpl.custom_users_master where user_id=:ratee_id)"
                        + " ) and feedback_status='Q'"
                        + " )"
                        + " ) a"
                        + " group by competency_name");
                query.setParameter("cycle_sequence_id", lngCycleSequenceId);
                query.setParameter("ratee_id", lngUserId);

                List list = query.list();
                return list;
            }
        });

    }

    @Override
    public List<Object[]> getImptbasedQuestionwiseAvg(Long lngUserId, Long lngCycleSequenceId) {

        return (List<Object[]>) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                SQLQuery query = session.createSQLQuery("select competency_name,question_text,round(avg(question_impt_rating),2)" + " from (" +
                        "select " +"(select competency_name from lntidpl.encompass_mstr_competency c where "
                        + "competency_id=(select q.competency_id from lntidpl.encompass_mstr_question q where q.question_id=t.question_id)) competency_name,"
                        +"(select q.question_text from lntidpl.encompass_mstr_question q where q.question_id=t.question_id) question_text," 
                        + "t.rater_id,question_impt_rating,question_freq_rating from lntidpl.encompass_trxn_feedback t " 
                        +"where cycle_sequence_id =:cycle_sequence_id and rater_id in " +"(" +
                        "select rater_id from lntidpl.encompass_mpng_cycle_sequence_ratee_rater where rater_id in (" +
                        "select user_id from lntidpl.custom_users_master where manager_full_name_lower_case=" +
                        "(select employee_full_name_lower_case from lntidpl.custom_users_master where user_id=:ratee_id)" +") and feedback_status='Q'" +
                        ")" +") a " +"group by competency_name,question_text");
                query.setParameter("cycle_sequence_id", lngCycleSequenceId);
                query.setParameter("ratee_id", lngUserId);

                List list = query.list();
                return list;
            }
        });

    }

    @Override
    public List<Object[]>  getSatisfactionbasedQuestionwiseAvg(Long lngUserId, Long lngCycleSequenceId) {
       return (List<Object[]>) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                SQLQuery query = session.createSQLQuery("select competency_name,question_text,round(avg(question_freq_rating),2)" + " from (" +
                        "select " +"(select competency_name from lntidpl.encompass_mstr_competency c where "
                        + "competency_id=(select q.competency_id from lntidpl.encompass_mstr_question q where q.question_id=t.question_id)) competency_name,"
                        +"(select q.question_text from lntidpl.encompass_mstr_question q where q.question_id=t.question_id) question_text," 
                        + "t.rater_id,question_impt_rating,question_freq_rating from lntidpl.encompass_trxn_feedback t " 
                        +"where cycle_sequence_id =:cycle_sequence_id and rater_id in " +"(" +
                        "select rater_id from lntidpl.encompass_mpng_cycle_sequence_ratee_rater where rater_id in (" +
                        "select user_id from lntidpl.custom_users_master where manager_full_name_lower_case=" +
                        "(select employee_full_name_lower_case from lntidpl.custom_users_master where user_id=:ratee_id)" +") and feedback_status='Q'" +
                        ")" +") a " +"group by competency_name,question_text");
                query.setParameter("cycle_sequence_id", lngCycleSequenceId);
                query.setParameter("ratee_id", lngUserId);

                List list = query.list();
                return list;
    }

   

});
 }

    @Override
    public List<Object[]> getSatisfactionbasedCompetencywiseAvg(Long lngUserId, Long lngCycleSequenceId) {
        return (List<Object[]>) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                SQLQuery query = session.createSQLQuery("select competency_name,round(avg(question_freq_rating),2)" + " from (" + "  select "
                        + " (select competency_name from lntidpl.encompass_mstr_competency c where competency_id=(select q.competency_id from lntidpl.encompass_mstr_question q where q.question_id=t.question_id)) competency_name,"
                        + " t.rater_id,question_impt_rating,question_freq_rating from lntidpl.encompass_trxn_feedback t"
                        + " where cycle_sequence_id =:cycle_sequence_id and rater_id in "
                        + "("
                        + " select rater_id from lntidpl.encompass_mpng_cycle_sequence_ratee_rater where rater_id in ("
                        + " select user_id from lntidpl.custom_users_master where manager_full_name_lower_case="
                        + " (select employee_full_name_lower_case from lntidpl.custom_users_master where user_id=:ratee_id)"
                        + " ) and feedback_status='Q'"
                        + " )"
                        + " ) a"
                        + " group by competency_name");
                query.setParameter("cycle_sequence_id", lngCycleSequenceId);
                query.setParameter("ratee_id", lngUserId);

                List list = query.list();
                return list;
            }
        });
    }
}
