/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.encompass.batch.dao;

import java.util.List;

/**
 *
 * @author cm-012
 */
public interface PrintReportDAO {

               List<Object[]> getStatuswiseResponseCount(Long lngUserId,Long lngCycleSequenceId);
               List<Object[]> getImptbasedCompetencywiseAvg(Long lngUserId,Long lngCycleSequenceId);
               List<Object[]> getImptbasedQuestionwiseAvg(Long lngUserId,Long lngCycleSequenceId);
               List<Object[]> getSatisfactionbasedQuestionwiseAvg(Long lngUserId,Long lngCycleSequenceId);
                List<Object[]> getSatisfactionbasedCompetencywiseAvg(Long lngUserId,Long lngCycleSequenceId);
    
    
}
