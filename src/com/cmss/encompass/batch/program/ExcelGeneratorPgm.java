/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cmss.encompass.batch.program;

/**
 *
 * @author cm-012
 */
import java.io.FileOutputStream;
import java.util.Iterator;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelGeneratorPgm {

    public int reportId;
    public String reportName;
    public String reportPath;

    public ExcelGeneratorPgm(int reportId) {
        this.reportId = reportId;
    }

    public void generateReport(String filePath, Object reportObject) throws Exception {
        Workbook workbook = null;
        reportName = reportId + ".xlsx";
        reportPath = filePath + reportName;
        
        if (reportName.endsWith("xlsx")) {
            System.out.println("the file type is xlsx");
            //workbook = new XSSFWorkbook();
        } else if (reportName.endsWith("xls")) {
            System.out.println("the file type is xls");
            workbook = new HSSFWorkbook();
        } else {
            throw new Exception("invalid file name, should be xls or xlsx");
        }

        Sheet sheet = workbook.createSheet(Integer.toString(reportId));

        List reportInfoList =(List) reportObject;
        System.out.println("list size is :"+reportInfoList.size());
        Iterator iterator = reportInfoList.iterator();
        int rowIndex = 0;
        while (iterator.hasNext()) {
//          Country country = iterator.next();
            iterator.next();
            Row row = sheet.createRow(rowIndex + 1);
            Cell cell0 = row.createCell(0);
//            cell0.setCellValue(country.getName());
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            cell0.setCellValue("janardhana");
            Cell cell1 = row.createCell(1);
            cell1.setCellValue("1234");
            System.out.println("###############");
        }

// lets write the excel data to file now
        FileOutputStream fos = new FileOutputStream(reportPath);
        workbook.write(fos);
        fos.close();
        System.out.println(reportName + " written successfully");
    }

}
